#include "helpers.h"

namespace astraCUDA3d {

__global__ void k_subtractVolumes(cudaPitchedPtr g, cudaPitchedPtr prev_g, float* sumOfSquares, unsigned int nX, unsigned int nY, unsigned int nZ) {
	// Performs g_k - g_(k-1)
	// The method wants g_k = Wt*R*(W*x -p), but we have computed Wt*R*(p-W*x) and assigned this to g_k,
	// so we are going to switch the operands to g_(k-1) - g_k. The result will be assigned to the now-useless g_(k-1)
	// It computes the sum of squares used for the gamma normalization, additionally.
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;
	
	char   	*char_pG     = ((char*)g.ptr      +j*g.pitch     ), 
			*char_pPrevG = ((char*)prev_g.ptr +j*prev_g.pitch);

	size_t slicePitch = g.pitch*nY; 
	float *pG, *pPrevG;

	float value=0.0f, threadSumOfSquares = 0.0f;

	if ((i<nX) && (j<nY)) {
		for (int k=0; k<nZ; ++k) {

			pG     = (float*) (char_pG +k*slicePitch);  pPrevG = (float*) (char_pPrevG +k*slicePitch);

			value = pPrevG[i] -pG[i];
			pPrevG[i] = value;
			threadSumOfSquares = threadSumOfSquares+ value*value;
		}
		atomicAdd(sumOfSquares, threadSumOfSquares);
	}
 }


__global__ void k_BBstep(cudaPitchedPtr x, cudaPitchedPtr prev_x, cudaPitchedPtr prev_g, float* norm_gradients, float* BBStepValue, unsigned int nX, unsigned int nY, unsigned int nZ){
	// Computes gamma 
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;

	char    *char_pX     = ((char*)x.ptr      +j*x.pitch     ), 
			*char_pPrevX = ((char*)prev_x.ptr +j*prev_x.pitch),  
			*char_pPrevG = ((char*)prev_g.ptr +j*prev_g.pitch);
	size_t slicePitch = x.pitch*nY; 
	float *pX, *pPrevX, *pPrevG;

	float threadSum=0.0f, norm=norm_gradients[0];

	if ((i<nX) && (j<nY)) {
		for (int k=0; k<nZ; ++k) {
			pX     = (float*) (char_pX +k*slicePitch);  pPrevX = (float*) (char_pPrevX +k*slicePitch); 
			pPrevG = (float*) (char_pPrevG +k*slicePitch);

			threadSum += (pX[i] - pPrevX[i])*pPrevG[i];
		}
		atomicAdd(BBStepValue, threadSum/norm);
	}
 }

__global__ void k_lastBBstep(cudaPitchedPtr x, cudaPitchedPtr prev_x, cudaPitchedPtr g, cudaPitchedPtr prev_g, float* BBStepValue, unsigned int nX, unsigned int nY, unsigned int nZ) {
	// Computes the net x_k+1 and assignes (x_k,g_k) to (x_k-1,g_k-1) to be used in the next iteration 
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;

	char    *char_pX     = ((char*)x.ptr      +j*x.pitch     ), 
			*char_pPrevX = ((char*)prev_x.ptr +j*prev_x.pitch),  
			*char_pG     = ((char*)g.ptr      +j*g.pitch     ), 
			*char_pPrevG = ((char*)prev_g.ptr +j*prev_g.pitch);
	size_t slicePitch = x.pitch*nY; 
	float *pX, *pPrevX, *pG, *pPrevG;

	float gamma = BBStepValue[0];
	if ((i<nX) && (j<nY)) {
		for (int k=0; k<nZ; ++k) {
			pX     = (float*) (char_pX +k*slicePitch);  pPrevX = (float*) (char_pPrevX +k*slicePitch); 
			pG     = (float*) (char_pG +k*slicePitch);  pPrevG = (float*) (char_pPrevG +k*slicePitch);

			pPrevX[i] = pX[i];
			pPrevG[i] = pG[i];
			pX[i] += gamma*pG[i];
		}
	}
 }

__global__ void mask(cudaPitchedPtr volumePtr, cudaPitchedPtr segmPtr, int nX, int nY, int nZ, 
	float* g_absorptionValues, int nMaterials ) { 
    //int nEnergyBins = spectralInfo.nY;

	size_t pitch = volumePtr.pitch;
	size_t slicePitch = pitch * nY; 
	size_t volumePitch = nZ*slicePitch;

	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;

	//* Copy data to shared memory
	extern __shared__ float s_absorptionValues [];

	s_absorptionValues[0]=0.0f;
	for (int idx=threadIdx.x + blockDim.x*threadIdx.y; idx<nMaterials; ){
			
			s_absorptionValues[idx+1]=g_absorptionValues[idx];

			idx+=blockDim.x*blockDim.y;
	}
	__syncthreads();

	float *voxelCurrentMaterial, *voxelPreviousMaterial, *refVoxel, refValue;
	char *currentVolumePtr, *refVolumePtr = (char*)volumePtr.ptr +j*pitch;

	if ((i<nX) && (j<nY)) {
		for (int k=0; k<nZ; ++k){
		
			currentVolumePtr = (char*)  segmPtr.ptr +j*pitch +k*slicePitch;
			refVoxel         = (float*) ( refVolumePtr     +k*slicePitch);
			refValue = refVoxel[i];
			
			if (refValue>s_absorptionValues[nMaterials]) {
				voxelCurrentMaterial = (float*) (currentVolumePtr +(nMaterials-1)*volumePitch);
				voxelCurrentMaterial[i] = refValue/s_absorptionValues[nMaterials]; 
			} else {
				for( int m=nMaterials-1; m>=0; --m ) {
					if (refValue>s_absorptionValues[m]){
						voxelCurrentMaterial = (float*) (currentVolumePtr  +(m-1)*volumePitch);
						voxelPreviousMaterial = (float*) (currentVolumePtr +m*volumePitch);
						voxelPreviousMaterial[i]=(refValue-s_absorptionValues[m])/(s_absorptionValues[m+1]-s_absorptionValues[m]); 
						if (m>0)
						voxelCurrentMaterial[i] =1.0f-voxelPreviousMaterial[i]; 
						m=-1;
					}
				}
			}
		}
	}
  }

__global__ void polyDARTmask(cudaPitchedPtr volumePtr, cudaPitchedPtr segmPtr, int nX, int nY, int nZ, 
	float* absorptionValuesPtr, float* absorptionThresholdsPtr, int nMaterials ) { 
    //int nEnergyBins = spectralInfo.nY;

	size_t pitch = volumePtr.pitch;
	size_t slicePitch = pitch * nY; 
	size_t volumePitch = nZ*slicePitch;

	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;

	//* Copy data to shared memory
	extern __shared__ float s_absorptionThresholds []; // [2*nMaterials] : [nMaterials] thresholds and [nMaterials] absorption values
	float *s_absorptionValues = &s_absorptionThresholds[nMaterials];

	//s_absorptionValues[0]=0.0f;
	for (int idx=threadIdx.x + blockDim.x*threadIdx.y; idx<nMaterials; ){
			
		s_absorptionThresholds[idx] = absorptionThresholdsPtr[idx];
		s_absorptionValues[idx]     = absorptionValuesPtr[idx];      //s_absorptionValues[idx+1]
		idx+=blockDim.x*blockDim.y;
	}
	__syncthreads();

	float *matVoxel, *refVoxel;
	char *currentVolumePtr, *refVolumePtr = (char*)volumePtr.ptr +j*pitch;

	if ((i<nX) && (j<nY)) {

		for (int k=0; k<nZ; ++k){
		
			currentVolumePtr = (char*)  segmPtr.ptr +j*pitch +k*slicePitch;
			refVoxel         = (float*) ( refVolumePtr     +k*slicePitch);
			
			for( int m=nMaterials-1; m>=0; --m ) {
				if (refVoxel[i]>s_absorptionThresholds[m]){
					matVoxel = (float*) (currentVolumePtr  +m*volumePitch);
					matVoxel[i] = s_absorptionValues[m];
					refVoxel[i] = s_absorptionValues[m]; // Override volume value
					m=-1;
				}
				else if (m==0) {
					refVoxel[i] = 0.0f; // Override volume value
				}
			}
		}    
	}
 }

bool AttenuationReferenceValues_byAverage(void* D_matAttenuations, void* D_avgMatAttenuations, const SMemoryDimensions3D& spectralInfo) {

	void* h_matAttenuations, *h_avgMatAttenuations;
	h_matAttenuations = malloc(sizeof(float)*spectralInfo.nX*spectralInfo.nY);
	cudaMemcpy ( h_matAttenuations, D_matAttenuations, sizeof(float)*spectralInfo.nX*spectralInfo.nY, cudaMemcpyDeviceToHost );
	h_avgMatAttenuations = malloc(sizeof(float)*spectralInfo.nX);

	int refColumn = spectralInfo.nY/2;

	float* hf_matAttenuations = (float*)h_matAttenuations, *hf_avgMatAttenuations = (float*)h_avgMatAttenuations ;
	for (int m=0; m<spectralInfo.nX; ++m){
		hf_avgMatAttenuations[m]=hf_matAttenuations[m*spectralInfo.nY +refColumn];
		//ASTRA_WARN("AttenuationReferenceValues_byAverage() ::: segmenting by value: %f\n", hf_avgMatAttenuations[m]);
	}

	cudaMemcpy ( D_avgMatAttenuations, h_avgMatAttenuations, sizeof(float)*spectralInfo.nX, cudaMemcpyHostToDevice );

	free(h_matAttenuations);
	free(h_avgMatAttenuations);
	return true;
	}

bool AttenuationReferenceValues_byWeightedAverage(void* D_energyResponse, void* D_matAttenuations, void* D_avgMatAttenuations, const SMemoryDimensions3D& spectralInfo) {

	int nMaterials=spectralInfo.nX, nEnergyBins=spectralInfo.nY; 
	void *h_matAttenuations, *h_avgMatAttenuations, *h_energyResponse;

	h_energyResponse = malloc(sizeof(float)*nEnergyBins);
	cudaMemcpy ( h_energyResponse, D_energyResponse, sizeof(float)*nEnergyBins, cudaMemcpyDeviceToHost );
	h_matAttenuations = malloc(sizeof(float)*nMaterials*nEnergyBins);
	cudaMemcpy ( h_matAttenuations, D_matAttenuations, sizeof(float)*nMaterials*nEnergyBins, cudaMemcpyDeviceToHost );
	h_avgMatAttenuations = malloc(sizeof(float)*nMaterials);
	
	float   *hf_matAttenuations    = (float*)h_matAttenuations, 
			*hf_avgMatAttenuations = (float*)h_avgMatAttenuations, 
			*hf_energyResponse     = (float*)h_energyResponse;

	for (int m=0; m<spectralInfo.nX; ++m){  
		// compute the sum of weights, aka energy bins
		float sum_of_weights = 0.0f;
		for( std::size_t e=0; e<nEnergyBins; ++e ) sum_of_weights += hf_energyResponse[e]*hf_matAttenuations[m*nEnergyBins +e];
		
		// compute the total of each score multiplied by its weight
		float weighted_total=0.0f;
		for( std::size_t e=0; e<nEnergyBins; ++e ) weighted_total += hf_energyResponse[e]*hf_matAttenuations[m*nEnergyBins +e]*e;

		int energyBin = (int) (weighted_total/sum_of_weights);
		hf_avgMatAttenuations[m]= hf_matAttenuations[m*nEnergyBins+energyBin];
		//ASTRA_WARN("AttenuationReferenceValues_byAverage() ::: %f %f\n", weighted_total, sum_of_weights);
		//ASTRA_WARN("AttenuationReferenceValues_byAverage() ::: segmenting by energy bin %d with value %f\n", energyBin, hf_avgMatAttenuations[m]);
	}

	cudaMemcpy ( D_avgMatAttenuations, h_avgMatAttenuations, sizeof(float)*spectralInfo.nX, cudaMemcpyHostToDevice );

	free(h_energyResponse);
	free(h_matAttenuations);
	free(h_avgMatAttenuations);
	return true;
	}

bool AttenuationReferenceValues_byLogAverage(void* D_energyResponse, void* D_matAttenuations, void* D_avgMatAttenuations, const SMemoryDimensions3D& spectralInfo) {
// GPU version
	int nMaterials=spectralInfo.nX, nEnergyBins=spectralInfo.nY; 
	void *h_matAttenuations, *h_avgMatAttenuations, *h_energyResponse;

	h_energyResponse     = malloc(sizeof(float)*nEnergyBins);
	cudaMemcpy ( h_energyResponse, D_energyResponse, sizeof(float)*nEnergyBins, cudaMemcpyDeviceToHost );
	h_matAttenuations    = malloc(sizeof(float)*nMaterials*nEnergyBins);
	cudaMemcpy ( h_matAttenuations, D_matAttenuations, sizeof(float)*nMaterials*nEnergyBins, cudaMemcpyDeviceToHost );
	h_avgMatAttenuations = malloc(sizeof(float)*nMaterials);
	
	float   *hf_matAttenuations    = (float*)h_matAttenuations, 
			*hf_avgMatAttenuations = (float*)h_avgMatAttenuations, 
			*hf_energyResponse     = (float*)h_energyResponse;

	cudaDeviceSynchronize();

	float sum_of_weights = 0.0f;
	for( std::size_t e=0; e<nEnergyBins; ++e ) sum_of_weights += hf_energyResponse[e];

	//for (std::size_t e=0; e<nEnergyBins; ++e ) ASTRA_WARN("....() ::: e: %d   ---   value: %f\n", e, hf_energyResponse[e]);


	for (int m=0; m<nMaterials; ++m){  

		float sum_of_exp_weights =0;
		for( std::size_t e=0; e<nEnergyBins; ++e ) sum_of_exp_weights += hf_energyResponse[e]*exp(-hf_matAttenuations[m*nEnergyBins +e]);

		hf_avgMatAttenuations[m]= -log(sum_of_exp_weights/sum_of_weights);
		//ASTRA_WARN("AttenuationReferenceValues_byLogAverage() ::: threshold  %d has value value %f", m, hf_avgMatAttenuations[m]);
	}

	cudaMemcpy ( D_avgMatAttenuations, h_avgMatAttenuations, sizeof(float)*nMaterials, cudaMemcpyHostToDevice );

	free(h_energyResponse);
	free(h_matAttenuations);
	free(h_avgMatAttenuations);
	return true;
	}

bool AttenuationReferenceValues_byLogAverage(const float* energyResponse, const float* matAttenuations, float* avgMatAttenuations, const SMemoryDimensions3D& spectralInfo) {
// CPU version
	int nMaterials=spectralInfo.nX, nEnergyBins=spectralInfo.nY;

	float sum_of_weights = 0.0f;
	for( std::size_t e=0; e<nEnergyBins; ++e ) sum_of_weights += energyResponse[e];

	//for (std::size_t e=0; e<nEnergyBins; ++e ) ASTRA_WARN("....() ::: e: %d   ---   value: %f\n", e, hf_energyResponse[e]);

	for (int m=0; m<nMaterials; ++m){  

		float sum_of_exp_weights =0;
		for( std::size_t e=0; e<nEnergyBins; ++e ) sum_of_exp_weights += energyResponse[e]*exp(-matAttenuations[m*nEnergyBins +e]);

		avgMatAttenuations[m]= -log(sum_of_exp_weights/sum_of_weights);
		ASTRA_WARN("AttenuationReferenceValues_byLogAverage() ::: mat %d has attenuation value %f", m, avgMatAttenuations[m]);
	}

	return true;
 }

bool SegmentationPolicy( cudaPitchedPtr D_volumeData2, cudaPitchedPtr D_tmpData,
	const SDimensions3D& dims2, 
	void* D_matAttenuations, void* D_energyResponse,
	const SMemoryDimensions3D& spectralInfo) {

	void* D_avgMatAttenuations;
	ASTRA_CUDA_ASSERT( cudaMalloc((void**)&D_avgMatAttenuations, sizeof(float)*spectralInfo.nX) );

	//AttenuationReferenceValues_byAverage(D_matAttenuations, D_avgMatAttenuations, spectralInfo);
	//AttenuationReferenceValues_byWeightedAverage(D_energyResponse, D_matAttenuations, D_avgMatAttenuations, spectralInfo);
	AttenuationReferenceValues_byLogAverage(D_energyResponse, D_matAttenuations, D_avgMatAttenuations, spectralInfo);

	//* Threads and blocks
	dim3 threadsPerBlock(32,8);
	dim3 numBlocks( (dims2.iVolX+threadsPerBlock.x-1)/threadsPerBlock.x, (dims2.iVolY+threadsPerBlock.y-1)/threadsPerBlock.y );

	//* Run kernels
	mask<<<numBlocks, threadsPerBlock, (spectralInfo.nX+1)*sizeof(float)>>>(D_volumeData2, D_tmpData, 
		dims2.iVolX, dims2.iVolY, dims2.iVolZ, 
		(float*)D_avgMatAttenuations, spectralInfo.nX );

	ASTRA_CUDA_ASSERT( cudaDeviceSynchronize() );
	ASTRA_CUDA_ASSERT( cudaFree(D_avgMatAttenuations) );
	return true;
 }

bool FindThresholdReferenceValues(const float* matAttenuations, const float* matPercThresholds, float* matThresholds, const SMemoryDimensions3D& spectralInfo) {

	int nMaterials=spectralInfo.nX; 

	if (matPercThresholds != NULL) { // User has supplied his own threshold percentages
		matThresholds[0]= matPercThresholds[0]*matAttenuations[0];
		for (int m=1; m<nMaterials; ++m){  
	
			matThresholds[m]= matAttenuations[m-1] + matPercThresholds[m]*(matAttenuations[m]-matAttenuations[m-1]);
			//ASTRA_WARN("FindThresholdReferenceValues() ::: mat: %d - mu_ref: %f\n", m, matThresholds[m]);
		}
	} else { // Find midpoints 
		matThresholds[0]= 0.5*matAttenuations[0];
		for (int m=1; m<nMaterials; ++m){  
	
			matThresholds[m]= matAttenuations[m-1] + 0.5*(matAttenuations[m]-matAttenuations[m-1]);
			//ASTRA_WARN("FindThresholdReferenceValues() ::: mat: %d - mu_ref: %f\n", m, matThresholds[m]);
		}
	}

	return true;
 }

bool PolyDART_SegmentationPolicy( cudaPitchedPtr D_volumeData2, cudaPitchedPtr D_tmpData, const SDimensions3D& dims2, 
	void* D_avgMatAttenuations, void* D_matThresholds, const SMemoryDimensions3D& spectralInfo) {

	//* Threads and blocks
	dim3 threadsPerBlock(32,8);
	dim3 numBlocks( (dims2.iVolX+threadsPerBlock.x-1)/threadsPerBlock.x, (dims2.iVolY+threadsPerBlock.y-1)/threadsPerBlock.y );

	//* Run kernels
	polyDARTmask<<<numBlocks, threadsPerBlock, 2*spectralInfo.nX*sizeof(float)>>>(D_volumeData2, D_tmpData, 
		dims2.iVolX, dims2.iVolY, dims2.iVolZ, 
		(float*)D_avgMatAttenuations, (float*)D_matThresholds, spectralInfo.nX );

	ASTRA_CUDA_ASSERT( cudaDeviceSynchronize() );
	return true;
	}

}
