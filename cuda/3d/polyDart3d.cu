/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
           2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include <cstdio>
#include <cassert>

#include "polyDart3d.h"
#include "util3d.h"
#include "arith3d.h"
#include "cone_fp.h"

#ifdef STANDALONE
#include "testutil.h"
#endif
#include "curand_kernel.h"

#define MAX_SIZE_FILTER_KERNEL 6 // Choose your max half kernel dimension, including the central voxel

namespace astraCUDA3d {

polyDART::polyDART() : ReconAlgo3D() {
	D_maskData.ptr = 0;
	D_smaskData.ptr = 0;

	D_sinoData.ptr = 0;
	D_volumeData.ptr = 0;

	D_projData.ptr = 0;
	D_tmpData.ptr = 0;

	D_lineWeight.ptr = 0;
	D_pixelWeight.ptr = 0;

	useVolumeMask = false;
	useSinogramMask = false;

	useMinConstraint = true;
	useMaxConstraint = false;

	initialPolySIRTIterations   = 1;
	nInternalPolySIRTiterations = 1;
	blurLength = 3;
	blurFactor   = 0.0f; 
	lambdaFactor = 1.0f;
	percentageOfFreeVoxels = 0.0f;

	isSpectralInfoReceived = false;
	applyBBstep = false;

	isFirstIteration = true;
 }


polyDART::~polyDART() {
	reset();
 }

void polyDART::reset() {
	cudaFree(D_projData.ptr);
	cudaFree(D_tmpData.ptr);
	cudaFree(D_mask.ptr);
	cudaFree(D_lineWeight.ptr);
	cudaFree(D_pixelWeight.ptr);
	cudaFree(D_energyResponse);
	cudaFree(D_matAttenuations);
	cudaFree(D_avgMatAttenuations);
	cudaFree(D_matThresholds);

	D_mask.ptr=0;

	D_maskData.ptr = 0;
	D_smaskData.ptr = 0;

	D_sinoData.ptr = 0;
	D_volumeData.ptr = 0;

	D_projData.ptr = 0;
	D_tmpData.ptr = 0;

	D_lineWeight.ptr = 0;
	D_pixelWeight.ptr = 0;

	D_energyResponse = 0;
	D_matAttenuations = 0;
	D_avgMatAttenuations = 0;
	D_matThresholds = 0;

	useVolumeMask = false;
	useSinogramMask = false;
	isSpectralInfoReceived = false;
	isFirstIteration = true;

	initialPolySIRTIterations   = 1;
	nInternalPolySIRTiterations = 1;
	blurLength = 3;
	blurFactor   = 0.0f; 
	lambdaFactor = 1.0f;
	percentageOfFreeVoxels = 0.0f;

	if (applyBBstep){
		cudaFree(D_prevVolumeData.ptr); 
		cudaFree(D_prevTmpData.ptr);
		D_prevVolumeData.ptr=0; 
		D_prevTmpData.ptr=0;
		applyBBstep = false;
	}

	ReconAlgo3D::reset();
 }

bool polyDART::enableVolumeMask() {
	useVolumeMask = true;
	return true;
 }

bool polyDART::enableSinogramMask(){
	useSinogramMask = true;
	return true;
 }


bool polyDART::init() {
	//ASTRA_WARN("polyDART::init() ::: Be not afeard; the isle is full of noises, ");

	D_pixelWeight = allocateVolumeData(dims);
	zeroVolumeData(D_pixelWeight, dims);

	D_mask = allocateVolumeData(dims);
	zeroVolumeData(D_mask, dims);

	//ASTRA_WARN("polyDART::init() ::: Sounds, and sweet airs, ");

	assert(isSpectralInfoReceived); // D_tmpData will be recycled for multiple purposes, so will have a different size
	multipleMaterialsDims = dims;
	multipleMaterialsDims.iVolZ *= spectralInfo.nX;
	D_tmpData = allocateVolumeData(multipleMaterialsDims);
	zeroVolumeData(D_tmpData, multipleMaterialsDims);

	//ASTRA_WARN("polyDART::init() ::: that give delight and hurt not. ");

	D_projData = allocateProjectionData(dims);
	zeroProjectionData(D_projData, dims);

	//ASTRA_WARN("polyDART::init() ::: Sometimes a thousand twangling instruments ");

	D_lineWeight = allocateProjectionData(dims);
	zeroProjectionData(D_lineWeight, dims);

	//ASTRA_WARN("polyDART::init() ::: Will hum about mine ears; ");

	// We can't precompute lineWeights and pixelWeights when using a mask
	if (!useVolumeMask && !useSinogramMask)
		precomputeWeights();

	//ASTRA_WARN("polyDART::init() ::: and sometime voices ");

	// TODO: check if allocations succeeded
	return true;
 }

bool polyDART::setMinConstraint(float fMin) {
	fMinConstraint = fMin;
	useMinConstraint = true;
	return true;
 }

bool polyDART::setMaxConstraint(float fMax) {
	fMaxConstraint = fMax;
	useMaxConstraint = true;
	return true;
 }

bool polyDART::setDARTparameters(int _initialPolySIRTIterations, int _nInternalPolySIRTiterations, int _blurLength,
	float _blurFactor, float _lambdaFactor, float _percentageOfFreeVoxels) {
	
	initialPolySIRTIterations = _initialPolySIRTIterations; 
	nInternalPolySIRTiterations = _nInternalPolySIRTiterations;
	blurLength = _blurLength;
	blurFactor = _blurFactor; 
	lambdaFactor = _lambdaFactor; 
	percentageOfFreeVoxels = _percentageOfFreeVoxels;	

	int semiBlurLength = blurLength/2;

	// checks
	if ( (blurLength%2) != 1 ){
		ASTRA_ERROR("The kernel of the blurring operation must be odd.");
		throw;
	}

	if ( ((mpiPrj->getGhostCells()).x <semiBlurLength) && ((mpiPrj->getGhostCells()).y <semiBlurLength)){
		ASTRA_ERROR("The kernel size of blurring operation is too low compared to the number of ghost voxels. Please, raise the number of ghost voxels.");
		throw;
	}
		

	return true;
 }

bool polyDART::precomputeWeights() {
	//ASTRA_WARN("polyDART::precomputeWeights() ::: FUll fathon five thy father lies; ");

	zeroProjectionData(D_lineWeight, dims);
	//ASTRA_WARN("polyDART::precomputeWeights() ::: 1");
	if (useVolumeMask) {
		//ASTRA_WARN("polyDART::precomputeWeights() ::: 2");
		callFP(D_maskData, D_lineWeight, 1.0f);
	} else {
		//ASTRA_WARN("polyDART::precomputeWeights() ::: 3");
		processVol3D<opSet>(D_tmpData, 1.0f, dims);
		//ASTRA_WARN("polyDART::precomputeWeights() ::: 4");
		callFP(D_tmpData, D_lineWeight, 1.0f);
	}

	//ASTRA_WARN("polyDART::precomputeWeights() ::: Of his bones are coral made; ");
	
	processSino3D<opInvert>(D_lineWeight, dims);

	if (useSinogramMask) {
		// scale line weights with sinogram mask to zero out masked sinogram pixels
		processSino3D<opMul>(D_lineWeight, D_smaskData, dims);
	}

	//ASTRA_WARN("polyDART::precomputeWeights() ::: Those are pearls that were his eyes ");

	zeroVolumeData(D_pixelWeight, dims);

	if (useSinogramMask) {
		callBP(D_pixelWeight, D_smaskData, 1.0f);
	} else {
		processSino3D<opSet>(D_projData, 1.0f, dims);
		callBP(D_pixelWeight, D_projData, 1.0f);
	}

	//ASTRA_WARN("polyDART::precomputeWeights() ::: Nothing of him that doth fade,");
	#if 0
	float* bufp = new float[512*512];

	for (int i = 0; i < 180; ++i) {
		for (int j = 0; j < 512; ++j) {
			cudaMemcpy(bufp+512*j, ((float*)D_projData.ptr)+180*512*j+512*i, 512*sizeof(float), cudaMemcpyDeviceToHost);
		}

		char fname[20];
		sprintf(fname, "ray%03d.png", i);
		saveImage(fname, 512, 512, bufp);
	}
	#endif

	#if 0
	float* buf = new float[256*256];

	for (int i = 0; i < 256; ++i) {
		cudaMemcpy(buf, ((float*)D_pixelWeight.ptr)+256*256*i, 256*256*sizeof(float), cudaMemcpyDeviceToHost);

		char fname[20];
		sprintf(fname, "pix%03d.png", i);
		saveImage(fname, 256, 256, buf);
	}
	#endif
	processVol3D<opInvert>(D_pixelWeight, dims);

	if (useVolumeMask) {
		// scale pixel weights with mask to zero out masked pixels
		processVol3D<opMul>(D_pixelWeight, D_maskData, dims);
	}
	//ASTRA_WARN("polyDART::precomputeWeights() ::: But doth suffer a sea change");
	//ASTRA_WARN("polyDART::precomputeWeights() ::: Into something rich and strange.");

	return true;
  }

bool polyDART::recomputeWeights() {
	// Recompute weights for the internal polySIRT iterations. This will help the update only of the free voxels

	zeroProjectionData(D_lineWeight, dims);

	callFP(D_mask, D_lineWeight, 1.0f);
	
	processSino3D<opInvert>(D_lineWeight, dims);

	zeroVolumeData(D_pixelWeight, dims);

	processSino3D<opSet>(D_projData, 1.0f, dims);
	callBP(D_pixelWeight, D_projData, 1.0f);
	
	processVol3D<opInvert>(D_pixelWeight, dims);

	// scale pixel weights with mask to zero out masked pixels
	processVol3D<opMul>(D_pixelWeight, D_mask, dims);

	return true;
  }

void polyDART::BBstep(cudaPitchedPtr x, cudaPitchedPtr prev_x, cudaPitchedPtr g, cudaPitchedPtr prev_g,  const SDimensions3D& dims) {

	//float norm_gradients=0.0f, BBStepValue=0.0f; // || g_k-g_(k-1) ||^2_2
	float BBStepValue=0.0f, norm_gradients=0.0f, *d_norm_gradients, *d_BBStepValue;
	cudaMalloc(&d_norm_gradients, sizeof(float));   cudaMalloc(&d_BBStepValue, sizeof(float)); 
	cudaMemset(d_norm_gradients, 0, sizeof(float)); cudaMemset(d_BBStepValue, 0, sizeof(float));

	#if 0 //debugcode
	//if (mpiPrj->getProcId()==2){
	SDimensions3D  dims3    = dims;
	float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
	copyVolumeFromDevice(pTmpData, x, dims3, 0);
	cudaTextForceKernelsCompletion();
	char buff[100];
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/x/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	copyVolumeFromDevice(pTmpData, prev_x, dims3, 0);
	cudaTextForceKernelsCompletion();
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/prev_x/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	copyVolumeFromDevice(pTmpData, g, dims3, 0);
	cudaTextForceKernelsCompletion();
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/g/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	copyVolumeFromDevice(pTmpData, prev_g, dims3, 0);
	cudaTextForceKernelsCompletion();
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/prev_g/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	free(pTmpData);
	//}
	#endif
	dim3 blockSize(32,8);
	dim3 gridSize((dims.iVolX+blockSize.x-1)/blockSize.x, (dims.iVolY+blockSize.y-1)/blockSize.y);

	k_subtractVolumes<<<gridSize, blockSize>>>(g, prev_g, d_norm_gradients, dims.iVolX, dims.iVolY, dims.iVolZ);
	cudaTextForceKernelsCompletion();

	cudaMemcpy(&norm_gradients, d_norm_gradients, sizeof(float), cudaMemcpyDeviceToHost);
	float global_norm_gradients=0.0f;
	MPI_Allreduce(&norm_gradients,&global_norm_gradients, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	cudaMemcpy(d_norm_gradients, &global_norm_gradients, sizeof(float), cudaMemcpyHostToDevice);

	if (isFirstIteration) {
		BBStepValue = 0.01f;
		cudaMemcpy(d_BBStepValue, &BBStepValue, sizeof(float), cudaMemcpyHostToDevice);
	} else {
		k_BBstep<<<gridSize, blockSize>>>(x, prev_x, prev_g, d_norm_gradients, d_BBStepValue, dims.iVolX, dims.iVolY, dims.iVolZ);
		cudaTextForceKernelsCompletion();	
	}

	cudaMemcpy(&BBStepValue, d_BBStepValue, sizeof(float), cudaMemcpyDeviceToHost);
	float global_BBStepValue=0.0f;
	MPI_Allreduce(&BBStepValue,&global_BBStepValue, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	if (mpiPrj->getProcId()==0)
		ASTRA_WARN("polySIRT::BBstep() ::: BB-step value: %f",global_BBStepValue);
	cudaMemcpy(d_BBStepValue, &global_BBStepValue, sizeof(float), cudaMemcpyHostToDevice);

	k_lastBBstep<<<gridSize, blockSize>>>(x, prev_x, g, prev_g, d_BBStepValue, dims.iVolX, dims.iVolY, dims.iVolZ);
	cudaTextForceKernelsCompletion();


	#if 0 //debugcode
	//if (mpiPrj->getProcId()==2){
	SDimensions3D  dims3    = dims;
	float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
	copyVolumeFromDevice(pTmpData, x, dims3, 0);
	cudaTextForceKernelsCompletion();
	char buff[100];
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/x/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	copyVolumeFromDevice(pTmpData, prev_x, dims3, 0);
	cudaTextForceKernelsCompletion();
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/prev_x/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	copyVolumeFromDevice(pTmpData, g, dims3, 0);
	cudaTextForceKernelsCompletion();
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/g/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	copyVolumeFromDevice(pTmpData, prev_g, dims3, 0);
	cudaTextForceKernelsCompletion();
	for (int k=0; k<dims3.iVolZ; ++k) {
		sprintf (buff, "/data/home/diuso/tmp/prev_g/slice_%d.raw", k);
		ofstream myFile (buff, ios::out | ios::binary);
		myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
		myFile.close();
	}
	free(pTmpData);
	//}
	#endif
		cudaFree(d_norm_gradients);  cudaFree(d_BBStepValue);
	}

bool polyDART::setVolumeMask(cudaPitchedPtr& _D_maskData) {
	assert(useVolumeMask);

	D_maskData = _D_maskData;

	return true;
	}

bool polyDART::setSinogramMask(cudaPitchedPtr& _D_smaskData) {
	assert(useSinogramMask);

	D_smaskData = _D_smaskData;

	return true;
	}

bool polyDART::setBuffers(cudaPitchedPtr& _D_volumeData, cudaPitchedPtr& _D_projData) {
		D_volumeData = _D_volumeData;
		D_sinoData = _D_projData;
	
		return true;
 }


__global__ void borderFinder(cudaPitchedPtr volumePtr, cudaPitchedPtr maskPtr, int nX, int nY, int nZ, int nMaterials, size_t volumePitch, size_t volByteToJump ) { 
  //int nEnergyBins = spectralInfo.nY;

  size_t pitch = volumePtr.pitch;
  size_t slicePitch = pitch * nY; 

  int i = blockIdx.x*blockDim.x + threadIdx.x;
  int j = blockIdx.y*blockDim.y + threadIdx.y;

  __syncthreads();
 
  float *voxelCurrentMaterial, *voxelCurrentMaterial1, *voxelCurrentMaterial2, *refVoxel;
  char  *currentVolumePtr  = (char*) volumePtr.ptr +volByteToJump +j*pitch,     *refVolumePtr      = (char*) maskPtr.ptr   +volByteToJump +j*pitch;
  char  *currentVolumePtr1 = (char*) volumePtr.ptr +volByteToJump +(j+1)*pitch, *currentVolumePtr2 = (char*) volumePtr.ptr +volByteToJump +(j-1)*pitch;  


  if ( (i>0) && (j>0) && (i<nX-1) && (j<nY-1)) {
    for (int m=0; m<nMaterials; ++m){

		for (int k=0; k<nZ; ++k){
			voxelCurrentMaterial  = (float*)  ( currentVolumePtr  +k*slicePitch +m*volumePitch );
			voxelCurrentMaterial1 = (float*)  ( currentVolumePtr1 +k*slicePitch +m*volumePitch );
			voxelCurrentMaterial2 = (float*)  ( currentVolumePtr2 +k*slicePitch +m*volumePitch );
			refVoxel              = (float*)  ( refVolumePtr      +k*slicePitch );

			bool ref = refVoxel[i], v0 =voxelCurrentMaterial[i], v1=voxelCurrentMaterial[i+1], v2=voxelCurrentMaterial[i-1], 
									v3=voxelCurrentMaterial1[i], v4=voxelCurrentMaterial2[i];

			refVoxel[i] =  ((v1^v0) | (v2^v0) | (v3^v0) | (v4^v0)) | ref;
		}
		__syncthreads();
	}
  }
  }

__global__ void randomSelect(cudaPitchedPtr maskPtr, int nX, int nY, int nZ, int rank, float randomAmount, size_t volByteToJump) { 
  //int nEnergyBins = spectralInfo.nY;

  size_t pitch = maskPtr.pitch;
  size_t slicePitch = pitch * nY; 
  size_t volumePitch = nZ*slicePitch;

  int i = blockIdx.x*blockDim.x + threadIdx.x;
  int j = blockIdx.y*blockDim.y + threadIdx.y;

  int idx = i+j*gridDim.x*blockDim.x, startingSeed = rank*nZ;

  curandState_t state;

  /* we have to initialize the state */
  curand_init(startingSeed, // the seed controls the start of the sequence. Each video-card will start where the previous finished
			  idx, // the sequence number is only important with multiple cores 
              0, // the offset is how much extra we advance in the sequence for each call, can be 0 
              &state);
 
  float *refVoxel, myrandf;
  char  *refVolumePtr = (char*)maskPtr.ptr +j*pitch+volByteToJump;

  __syncthreads();

  if ( (i<nX) && (j<nY)) {
	for (int k=0; k<nZ; ++k){

		refVoxel = (float*)   ( refVolumePtr +k*slicePitch );
		myrandf  = curand_uniform(&state);
		if (myrandf<randomAmount) 
			refVoxel[i] = 1.0f;
	}
  }
  }


bool MaskingPolicy(cudaPitchedPtr& D_tmpData, cudaPitchedPtr& D_mask, const SDimensions3D& dims, int nMaterials, float percentageOfFreeVoxels, const astra::CMPIProjector3D *mpiPrj = NULL) {
	
	size_t zoffset=0, volByteToJump=0, projByteToJump=0;
	cudaPitchedPtr D_redTmpData = D_tmpData;
	SDimensions3D dimsWG = dims; // dimensions without ghost cells

	SDimensions3D  dimsAllMaterials = dims;
	dimsAllMaterials.iVolZ*=nMaterials;
	dimsAllMaterials.iProjV*=nMaterials;

	size_t volumePitch=  D_redTmpData.pitch * D_redTmpData.ysize * dims.iVolZ;

	#if USE_MPI
	if(mpiPrj) {
		//Modify the volume properties to ignore the ghostcells
		//Modify the height, and change the startpoint of the copy (zoffset)
		int2 ghosts = mpiPrj->getGhostCells();
		dimsWG.iVolZ -= (ghosts.x + ghosts.y);
		zoffset     = ghosts.x;
		volByteToJump      = ghosts.x * D_redTmpData.pitch * D_redTmpData.ysize;
		volumePitch =  D_redTmpData.pitch * D_redTmpData.ysize * dims.iVolZ - volByteToJump;

		//D_redTmpData.ptr = (char*)D_redTmpData.ptr +volByteToJump;
	}
	#endif

	dim3 blockSize(32,8);
	dim3 gridSize((dims.iVolX+blockSize.x-1)/blockSize.x, (dims.iVolY+blockSize.y-1)/blockSize.y);
	
	borderFinder<<<gridSize, blockSize>>>(D_redTmpData, D_mask, dimsWG.iVolX, dimsWG.iVolY, dimsWG.iVolZ, nMaterials, volumePitch, volByteToJump );
	randomSelect<<<gridSize, blockSize>>>(D_mask, dimsWG.iVolX, dimsWG.iVolY, dimsWG.iVolZ, mpiPrj->getProcId(), percentageOfFreeVoxels, volByteToJump );

	return true;
  }

__global__ void applyMask(cudaPitchedPtr volPtr, cudaPitchedPtr maskPtr, int nX, int nY, int nZ, size_t volByteToJump) { 
	//int nEnergyBins = spectralInfo.nY;
  
	size_t pitch = maskPtr.pitch;
	size_t slicePitch = pitch * nY; 
  
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;
  
   
	float *maskVoxel, *volVoxel;
	char  *maskCurrentPtr = (char*)maskPtr.ptr +j*pitch+volByteToJump, *volCurrentPtr = (char*)volPtr.ptr +j*pitch+volByteToJump;
  
	__syncthreads();
  
	int cnt=1, syncStep = 10;
	
	if ( (i<nX) && (j<nY)) {
		for (int k=0; k<nZ; ++k){
	
			volVoxel  = (float*)   ( volCurrentPtr  +k*slicePitch );
			maskVoxel = (float*)   ( maskCurrentPtr +k*slicePitch );
			if (maskVoxel[i]) {
				maskVoxel[i] = 0.0f;
				//volVoxel[i]  = 1.0f; // debug line
			} else {
				maskVoxel[i] = volVoxel[i];
				//maskVoxel[i] = 1.0f; // debug line
				volVoxel[i]  = 0.0f;
			}
			
			if (cnt == syncStep){
				cnt = 0;
				__syncthreads();
			}
			++cnt;
		}
	}
  }

bool splitVolume(cudaPitchedPtr& D_volumeData, cudaPitchedPtr& D_mask, const SDimensions3D& dims, const astra::CMPIProjector3D *mpiPrj = NULL) {

	size_t zoffset=0, volByteToJump=0, projByteToJump=0;
	SDimensions3D dimsWG = dims; // dimensions without ghost cells

	#if USE_MPI
	if(mpiPrj) {
		//Modify the volume properties to ignore the ghostcells
		//Modify the height, and change the startpoint of the copy (zoffset)
		int2 ghosts = mpiPrj->getGhostCells();
		dimsWG.iVolZ -= (ghosts.x + ghosts.y);
		zoffset     = ghosts.x;
		volByteToJump      = ghosts.x * D_volumeData.pitch * D_volumeData.ysize;
	}
	#endif

	dim3 blockSize(32,8);
	dim3 gridSize((dims.iVolX+blockSize.x-1)/blockSize.x, (dims.iVolY+blockSize.y-1)/blockSize.y);

	applyMask<<<gridSize, blockSize>>>( D_volumeData, D_mask, dimsWG.iVolX, dimsWG.iVolY, dimsWG.iVolZ, volByteToJump );

	return true;
  }

__global__ void blur(cudaPitchedPtr volDataPtr, void* kerDataPtr, int nX, int nY, int nZ, int kLen) { 
	//int nEnergyBins = spectralInfo.nY;
  
	size_t pitch = volDataPtr.pitch;
	size_t slicePitch = pitch * nY; 
  
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;
  
	char  *volPtr = (char*)volDataPtr.ptr;
	float *volVoxel, *kerVoxel = (float*) kerDataPtr;

	__syncthreads();
  
	int cnt=0;                    				// circular buffer counter
	int kSemiLen = kLen/2;        				// rounds on floor
	float convMemory[MAX_SIZE_FILTER_KERNEL]; 	// circular buffer

	for (int idx=0;idx<kSemiLen+1; ++idx) convMemory[idx]=0.0f;
	
	if ( (i>=kSemiLen) && (j>=kSemiLen) && (i<nX-kSemiLen) && (j<nY-kSemiLen)) { // sacrifies border voxels for easier development of code
		for (int k=kSemiLen; k<nZ-kSemiLen; ++k){
			
			/***** Perform Convolution *****/
			convMemory[cnt] = 0.0f;
			for(int z = 0; z < kLen; ++z) {
				for(int y = 0; y < kLen; ++y) {
					for(int x = 0; x < kLen; ++x) {

						volVoxel = (float*) (volPtr +(j+y-kSemiLen)*pitch +(k+z-kSemiLen)*slicePitch);
						convMemory[cnt] += volVoxel[i+x-kSemiLen] * kerVoxel[x +y*kLen +z*kLen*kLen];
					}
				}
			}
						
			__syncthreads();
			volVoxel = (float*) (volPtr +j*pitch +(k-kSemiLen)*slicePitch);
			cnt = (cnt-kSemiLen) ? cnt+1 : 0;
			//volVoxel[i] = convMemory[cnt];
			//volVoxel[i] += (convMemory[cnt]-volVoxel[i]*kerVoxel[kSemiLen +kSemiLen*kLen +kSemiLen*kLen*kLen]);
			volVoxel[i] += convMemory[cnt];
			//volVoxel[i] = 0.5*volVoxel[i]*(1-kerVoxel[kSemiLen +kSemiLen*kLen +kSemiLen*kLen*kLen]) +0.5*convMemory[cnt]/(kLen*kLen*kLen-1); //debug
		}
		for (int k=nZ-kSemiLen; k<nZ; ++k){ // copies the last convolution results from buffer to the volume
			volVoxel = (float*) (volPtr +j*pitch +(k-kSemiLen)*slicePitch);
			cnt = (cnt-kSemiLen) ? cnt+1 : 0;
			//volVoxel[i] = convMemory[cnt];
			volVoxel[i] += convMemory[cnt]; //debug
		}
	}
  }

bool createGaussianKernel(float* filterKernel, float blurFactor, int kLen){

	int kSemiLen = kLen/2;

	for(int z = 0; z < kLen; ++z)
		for(int y = 0; y < kLen; ++y)
			for(int x = 0; x < kLen; ++x) {
				filterKernel[x +y*kLen +z*kLen*kLen] = exp(-( pow(x-kSemiLen,2)+pow(y-kSemiLen,2)+pow(z-kSemiLen,2) ) / 
																	(2*blurFactor*blurFactor)) /
																	(blurFactor*pow(2*3.141592,0.5));
				//ASTRA_WARN("# %d,%d,%d # :: %f", x,y,z,filterKernel[x +y*kLen +z*kLen*kLen]);
			}

	return true; 
    }
bool createGaussianKernelZeroMean(float* filterKernel, float blurFactor, int kLen){

	int kSemiLen = kLen/2;

	for(int z = 0; z < kLen; ++z)
		for(int y = 0; y < kLen; ++y)
			for(int x = 0; x < kLen; ++x) {
				filterKernel[x +y*kLen +z*kLen*kLen] = exp(-( pow(x-kSemiLen,2)+pow(y-kSemiLen,2)+pow(z-kSemiLen,2) ) / 
																	(2*blurFactor*blurFactor)) /
																	(pow(2*3.141592*blurFactor*blurFactor,1.5));
				//ASTRA_WARN("# %d,%d,%d # :: %f", x,y,z,filterKernel[x +y*kLen +z*kLen*kLen]);
			}

	filterKernel[kSemiLen +kSemiLen*kLen +kSemiLen*kLen*kLen]=0; // debug

	return true; 
	}

bool createConstantKernel(float* filterKernel, float blurFactor, int kLen){

	int kSemiLen = kLen/2;
	int norm = 1/(kLen*kLen*kLen-1); //debug

	for(int z = 0; z < kLen; ++z)
		for(int y = 0; y < kLen; ++y)
			for(int x = 0; x < kLen; ++x) {
				filterKernel[x +y*kLen +z*kLen*kLen] = norm;
				//ASTRA_WARN("# %d,%d,%d # :: %f", x,y,z,filterKernel[x +y*kLen +z*kLen*kLen]);
			}

	filterKernel[kSemiLen +kSemiLen*kLen +kSemiLen*kLen*kLen]=0; // debug

	return true; 
	}

bool blurringPolicy(cudaPitchedPtr& D_volumeData, float blurFactor, int blurLength, const SDimensions3D& dims, const astra::CMPIProjector3D *mpiPrj = NULL) {

	size_t zoffset=0, volByteToJump=0, projByteToJump=0;
	SDimensions3D dimsWG = dims; // dimensions without ghost cells

	#if USE_MPI
	if(mpiPrj) {
		//Modify the volume properties to ignore the ghostcells
		//Modify the height, and change the startpoint of the copy (zoffset)
		int2 ghosts = mpiPrj->getGhostCells();
		dimsWG.iVolZ -= (ghosts.x + ghosts.y);
		zoffset     = ghosts.x;
		volByteToJump      = ghosts.x * D_volumeData.pitch * D_volumeData.ysize;
	}
	#endif


	float filterKernel[blurLength*blurLength*blurLength];
	//createGaussianKernel(filterKernel,blurFactor, blurLength);
	//createConstantKernel(filterKernel,blurFactor, blurLength);
	createGaussianKernelZeroMean(filterKernel,blurFactor, blurLength);
	void* D_kernel; 
	cudaMalloc(&D_kernel, blurLength*blurLength*blurLength*sizeof(int));
	cudaMemcpy(D_kernel, filterKernel, blurLength*blurLength*blurLength*sizeof(float), cudaMemcpyHostToDevice);
	
	dim3 blockSize(32,8);
	dim3 gridSize((dims.iVolX+blockSize.x-1)/blockSize.x, (dims.iVolY+blockSize.y-1)/blockSize.y);

	blur<<<gridSize, blockSize>>>( D_volumeData, (void*)D_kernel, dims.iVolX, dims.iVolY, dims.iVolZ, blurLength );

	cudaFree(D_kernel);

	return true;
	}




bool polyDART::iterate(unsigned int iterations) {
	if (mpiPrj->getProcId()==0) ASTRA_WARN("polyDART::iterate()");

	int nMaterials = spectralInfo.nX;
	SDimensions3D  dimsAllMaterials = dims;
	dimsAllMaterials.iVolZ *=nMaterials;
	dimsAllMaterials.iProjV*=nMaterials;

	// vvvvvv | Perform a some iterations, if needed
	polySIRTiterate(initialPolySIRTIterations, false);

	zeroVolumeData(D_tmpData, dimsAllMaterials);
	PolyDART_SegmentationPolicy(D_volumeData, D_tmpData, dims, D_avgMatAttenuations, D_matThresholds, spectralInfo);


	#if 1 //debugcode
		SDimensions3D  dims3    = dims;
		//dims3.iVolZ *= nMaterials;
		float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
		copyVolumeFromDevice(pTmpData, D_volumeData, dims3, 0);
		cudaTextForceKernelsCompletion();
		char buff[100];
		for (int k=0; k<dims3.iVolZ; ++k) {
			sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
			ofstream myFile (buff, ios::out | ios::binary);
			myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
			myFile.close();
		}
		free(pTmpData);
	#endif

	for (int iter=0; iter<iterations; ++iter) {
		if (mpiPrj->getProcId()==0) ASTRA_WARN("polyDART::iterate() ::: iteration %d of %d",iter+1,iterations);

		// vvvvvv | Masking
		zeroVolumeData(D_mask, dims);
		MaskingPolicy(D_tmpData, D_mask, dims, nMaterials, percentageOfFreeVoxels, mpiPrj);

		recomputeWeights(); // Recompute weights for the internal polySIRT iterations. This will help the update only of the free voxels

		#if 0 //debugcode
			SDimensions3D  dims3    = dims;
			//dims3.iVolZ *= nMaterials;
			float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
			copyVolumeFromDevice(pTmpData, D_mask, dims3, 0);
			cudaTextForceKernelsCompletion();
			char buff[100];
			for (int k=0; k<dims3.iVolZ; ++k) {
				sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
				ofstream myFile (buff, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
				myFile.close();
			}
			free(pTmpData);
		#endif

		// vvvvvv | internal polySIRT 
		polySIRTiterate(nInternalPolySIRTiterations, true); // should be true for internal polysirt
		splitVolume(D_volumeData, D_mask, dims, mpiPrj);    // D_mask will keep the the value "fixed voxels" from now on

		#if 0 //debugcode
			SDimensions3D  dims3    = dims;
			//dims3.iVolZ *= nMaterials;
			float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
			copyVolumeFromDevice(pTmpData, D_mask, dims3, 0);
			cudaTextForceKernelsCompletion();
			char buff[100];
			for (int k=0; k<dims3.iVolZ; ++k) {
				sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
				ofstream myFile (buff, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
				myFile.close();
			}
			free(pTmpData);
		#endif

		#if 0 //debugcode
			SDimensions3D  dims4    = dims;
			//dims4.iVolZ *= nMaterials;
			float *pTmpData2= (float*)malloc(dims4.iVolZ*dims4.iVolY*dims4.iVolX*sizeof(float));
			copyVolumeFromDevice(pTmpData2, D_volumeData, dims4, 0);
			cudaTextForceKernelsCompletion();
			char buff2[100];
			for (int k=0; k<dims4.iVolZ; ++k) {
				sprintf (buff2, "/home/snip/tmp/%d/vol1/%d.raw", mpiPrj->getProcId(), k);
				ofstream myFile (buff2, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData2[k*dims4.iVolY*dims4.iVolX], dims4.iVolY*dims4.iVolX*sizeof(float));
				myFile.close();
			}
			free(pTmpData2);
		#endif
		
		#if USE_MPI
		if(mpiPrj)
		{
			const_cast<astra::CMPIProjector3D*>(mpiPrj)->exchangeOverlapAndGhostRegions(
					NULL, D_volumeData, false, 0);
		}
		#endif

		#if 0 //debugcode
			SDimensions3D  dims3    = dims;
			//dims3.iVolZ *= nMaterials;
			float *pTmpData= (float*)malloc(dims3.iVolZ*dims3.iVolY*dims3.iVolX*sizeof(float));
			copyVolumeFromDevice(pTmpData, D_volumeData, dims3, 0);
			cudaTextForceKernelsCompletion();
			char buff[100];
			for (int k=0; k<dims3.iVolZ; ++k) {
				sprintf (buff, "/home/snip/tmp/%d/vol/%d.raw", mpiPrj->getProcId(), k);
				ofstream myFile (buff, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData[k*dims3.iVolY*dims3.iVolX], dims3.iVolY*dims3.iVolX*sizeof(float));
				myFile.close();
			}
			free(pTmpData);
		#endif

		// vvvvvv | Blur
		blurringPolicy(D_volumeData, blurFactor, blurLength, dims, mpiPrj);

		#if 0 //debugcode
			SDimensions3D  dims4    = dims;
			//dims4.iVolZ *= nMaterials;
			float *pTmpData3= (float*)malloc(dims4.iVolZ*dims4.iVolY*dims4.iVolX*sizeof(float));
			copyVolumeFromDevice(pTmpData3, D_volumeData, dims4, 0);
			cudaTextForceKernelsCompletion();
			char buff3[100];
			for (int k=0; k<dims4.iVolZ; ++k) {
				sprintf (buff3, "/home/snip/tmp/%d/vol1/%d.raw", mpiPrj->getProcId(), k);
				ofstream myFile (buff3, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData3[k*dims4.iVolY*dims4.iVolX], dims4.iVolY*dims4.iVolX*sizeof(float));
				myFile.close();
			}
			free(pTmpData3);
		#endif

		// vvvvvv | Sum blurred volume and fixed-volume
		processVol3D<opAddScaled>(D_volumeData, D_mask, 1.0f, dims);

		// vvvvvv | Sperimental blur
		//blurringPolicy(D_volumeData, blurFactor, blurLength, dims, mpiPrj);

		// vvvvvv | Segmentation
		zeroVolumeData(D_tmpData, dimsAllMaterials);
		PolyDART_SegmentationPolicy(D_volumeData, D_tmpData, dims, D_avgMatAttenuations, D_matThresholds, spectralInfo);


		#if 0 //debugcode
			SDimensions3D  dims5    = dims;
			//dims5.iVolZ *= nMaterials;
			float *pTmpData5= (float*)malloc(dims5.iVolZ*dims5.iVolY*dims5.iVolX*sizeof(float));
			copyVolumeFromDevice(pTmpData5, D_volumeData, dims5, 0);
			cudaTextForceKernelsCompletion();
			char buff5[100];
			for (int k=0; k<dims5.iVolZ; ++k) {
				sprintf (buff5, "/home/snip/tmp/%d/vol2/%d.raw", mpiPrj->getProcId(), k);
				ofstream myFile (buff5, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData5[k*dims5.iVolY*dims5.iVolX], dims5.iVolY*dims5.iVolX*sizeof(float));
				myFile.close();
			}
			free(pTmpData5);
		#endif
		/* debug
		if (mpiPrj->getProcId() == 1){
			cudaMemset3D( D_volumeData, 1.0f, make_cudaExtent(dims.iVolX*sizeof(float),dims.iVolY,dims.iVolZ) ) ;
			//zeroVolumeData(D_volumeData, dims); //debug
			//processVol3D<opAdd>(D_volumeData, 0.01f, dims);
		}*/
			
	}

	return true;
 }

bool polyDART::polySIRTiterate(unsigned int iterations, bool isInternalLoop) {
	shouldAbort = false;
	

	// iteration
	for (unsigned int iter = 0; iter < iterations && !shouldAbort; ++iter) {
		// copy sinogram to projection data
		if (mpiPrj->getProcId()==0) ASTRA_WARN("polySIRTiterate::iterate() ::: iteration %d of %d",iter+1,iterations);

	#if 1// MPI
        //zeroProjectionData(D_projData, dims); // it occurs inside polyFP now
	#else
        duplicateProjectionData(D_projData, D_sinoData, dims);
	#endif

		// compute - W * x
		//
		callPolyFP(D_volumeData, D_tmpData, D_projData, -1.0f, D_energyResponse, D_matAttenuations, spectralInfo);

	#if 0 //debugcode
		if (mpiPrj->getProcId()==0){
			SDimensions3D  dims3    = dims;
			float *pTmpData= (float*)malloc(dims3.iProjU*dims3.iProjAngles*dims3.iProjV*sizeof(float));
			copyProjectionsFromDevice(pTmpData, D_projData, dims3);
			cudaTextForceKernelsCompletion();
			char buff[100];
			for (int k=0; k<dims3.iProjV; ++k) {
				sprintf (buff, "/data/home/diuso/tmp/FP/proj%d.raw", k);
				ofstream myFile (buff, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData[k*dims3.iProjU*dims3.iProjAngles], dims3.iProjU*dims3.iProjAngles*sizeof(float));
				myFile.close();
			}
			free(pTmpData);
		}
	#endif 

	#if 1 // MPI
		// compute (p - W*x)
        processSino3D<opAddScaled>(D_projData, D_sinoData, 1.0f, dims);
	#endif

	#if 0 //debugcode
		if (mpiPrj->getProcId()==1){
			SDimensions3D  dims3    = dims;
			float *pTmpData= (float*)malloc(dims3.iProjU*dims3.iProjAngles*dims3.iProjV*sizeof(float));
			copyProjectionsFromDevice(pTmpData, D_projData, dims3);
			cudaTextForceKernelsCompletion();
			char buff[100];
			for (int k=0; k<dims3.iProjV; ++k) {
				sprintf (buff, "/data/home/diuso/tmp/proj_difference/proj_%d.raw", k);
				ofstream myFile (buff, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData[k*dims3.iProjU*dims3.iProjAngles], dims3.iProjU*dims3.iProjAngles*sizeof(float));
				myFile.close();
			}
			free(pTmpData);
		}
	#endif

		// compute R*(p - W*x)
		processSino3D<opMul>(D_projData, D_lineWeight, dims);

	#if 0 //debugcode
		if (mpiPrj->getProcId()==1){
			SDimensions3D  dims3    = dims;
			float *pTmpData= (float*)malloc(dims3.iProjU*dims3.iProjAngles*dims3.iProjV*sizeof(float));
			copyProjectionsFromDevice(pTmpData, D_projData, dims3);
			cudaTextForceKernelsCompletion();
			char buff[100];
			for (int k=0; k<dims3.iProjV; ++k) {
				sprintf (buff, "/data/home/diuso/tmp/proj_weighted/proj_%d.raw", k);
				ofstream myFile (buff, ios::out | ios::binary);
				myFile.write ((char*)&pTmpData[k*dims3.iProjU*dims3.iProjAngles], dims3.iProjU*dims3.iProjAngles*sizeof(float));
				myFile.close();
			}
			free(pTmpData);
		}
	#endif
		// compute Wt*R*(p - W*x)
		//
		zeroVolumeData(D_tmpData, dims);
		callBP(D_tmpData, D_projData, 1.0f);

		//if (isInternalLoop) refuso del passato, ora è D_pixelWeight che maschera a dovere
		//	processVol3D<opMul>(D_tmpData, D_mask, dims);

		if (!applyBBstep) {

			// compute C*Wt*R*(p - W*x)
			//
			processVol3D<opAddMulScaled>(D_volumeData, D_tmpData, D_pixelWeight,lambdaFactor, dims);

		} else {

			// In case we use BB, we should mask out the fixed voxels to avoid their update
			if (isInternalLoop)
				processVol3D<opMul>(D_tmpData, D_mask, dims);
			
			// ### stai attento ai segni ###
			//
			// x, x_prev, g, g_prev
			//if (mpiPrj->getProcId()==1)
			BBstep(D_volumeData, D_prevVolumeData, D_tmpData, D_prevTmpData, dims);
		}

		
		if (useMinConstraint)
			processVol3D<opClampMin>(D_volumeData, fMinConstraint, dims);
		if (useMaxConstraint)
			processVol3D<opClampMax>(D_volumeData, fMaxConstraint, dims);

		isFirstIteration=false;
	}

	return true;
 }

float polyDART::computeDiffNorm() {
	// copy sinogram to projection data
	duplicateProjectionData(D_projData, D_sinoData, dims);

	// do FP, subtracting projection from sinogram
	if (useVolumeMask) {
			duplicateVolumeData(D_tmpData, D_volumeData, dims);
			processVol3D<opMul>(D_tmpData, D_maskData, dims);
			callFP(D_tmpData, D_projData, -1.0f);
	} else {
			callFP(D_volumeData, D_projData, -1.0f);
	}

	float s = dotProduct3D(D_projData, dims.iProjU, dims.iProjAngles, dims.iProjV);
	return sqrt(s);
 }

bool polyDART::SetSpectralInformation(const float* pfEnergyResponse, const float* pfMatAttenuations, const float* pfMatPercThresholds, const SMemoryDimensions3D& spectralInfo_) {
	spectralInfo = spectralInfo_;
	int nMaterials=spectralInfo.nX, nEnergyBins=spectralInfo.nY;

	if (!isSpectralInfoReceived) {
		cudaMalloc((void**)&D_energyResponse, sizeof(float)*nEnergyBins);
		cudaMemcpy(D_energyResponse, (void*) pfEnergyResponse, sizeof(float)*nEnergyBins, cudaMemcpyHostToDevice);

		cudaMalloc((void**)&D_matAttenuations,sizeof(float)*nMaterials*nEnergyBins);
		cudaMemcpy(D_matAttenuations, (void*) pfMatAttenuations, sizeof(float)*nMaterials*nEnergyBins, cudaMemcpyHostToDevice);

		float *pfAvgMatAttenuations = new float [nMaterials];
		AttenuationReferenceValues_byLogAverage(pfEnergyResponse, pfMatAttenuations, pfAvgMatAttenuations, spectralInfo);
		cudaMalloc((void**)&D_avgMatAttenuations,sizeof(float)*nMaterials);
		cudaMemcpy(D_avgMatAttenuations, (void*) pfAvgMatAttenuations, sizeof(float)*nMaterials, cudaMemcpyHostToDevice);

		float *pfMatThresholds = new float [nMaterials];
		FindThresholdReferenceValues(pfAvgMatAttenuations, pfMatPercThresholds, pfMatThresholds, spectralInfo); 
		cudaMalloc((void**)&D_matThresholds,sizeof(float)*nMaterials);
		cudaMemcpy(D_matThresholds, (void*) pfMatThresholds, sizeof(float)*nMaterials, cudaMemcpyHostToDevice);

		delete [] pfAvgMatAttenuations;
		delete [] pfMatThresholds;

		isSpectralInfoReceived = true;

	} else { // Updating data - No allocation needed
		cudaMemcpy(D_energyResponse, (void*) pfEnergyResponse, sizeof(float)*nEnergyBins, cudaMemcpyHostToDevice);
		cudaMemcpy(D_matAttenuations, (void*) pfMatAttenuations, sizeof(float)*nMaterials*nEnergyBins, cudaMemcpyHostToDevice);

		float *pfAvgMatAttenuations = new float [nMaterials];
		AttenuationReferenceValues_byLogAverage(pfEnergyResponse, pfMatAttenuations, pfAvgMatAttenuations, spectralInfo);
		cudaMemcpy(D_avgMatAttenuations, (void*) pfAvgMatAttenuations, sizeof(float)*nMaterials, cudaMemcpyHostToDevice);

		float *pfMatThresholds = new float [nMaterials];
		FindThresholdReferenceValues(pfAvgMatAttenuations, pfMatPercThresholds, pfMatThresholds, spectralInfo); 
		cudaMemcpy(D_matThresholds, (void*) pfMatThresholds, sizeof(float)*nMaterials, cudaMemcpyHostToDevice);

		delete [] pfAvgMatAttenuations;
		delete [] pfMatThresholds;
	}
	
	return true;
 }

bool polyDART::SetBBStepOptimization(bool value) {
	applyBBstep = value;

	D_prevVolumeData = allocateVolumeData(dims);
	D_prevTmpData    = allocateVolumeData(dims);

	zeroVolumeData(D_prevVolumeData, dims);
	zeroVolumeData(D_prevTmpData, dims);

	return true;
 }


 // FIXME: to update
bool doPolyDART(cudaPitchedPtr& D_volumeData, 
            cudaPitchedPtr& D_sinoData,
            cudaPitchedPtr& D_maskData,
            const SDimensions3D& dims, const SConeProjection* angles,
            unsigned int iterations) {
	polyDART sirt;
	bool ok = true;

	ok &= sirt.setConeGeometry(dims, angles, 1.0f);
	if (D_maskData.ptr)
		ok &= sirt.enableVolumeMask();

	if (!ok)
		return false;

	ok = sirt.init();
	if (!ok)
		return false;

	if (D_maskData.ptr)
		ok &= sirt.setVolumeMask(D_maskData);

	ok &= sirt.setBuffers(D_volumeData, D_sinoData);
	if (!ok)
		return false;

	ok = sirt.iterate(iterations);

	return ok;
 }




}