#include <cstdio>
#include <cassert>
#include <cuda.h>

#include "dims3d.h"
#include "mpi.h"
#include "algo3d.h"
#include "arith3d.h"

#ifndef _CUDA_HELPERS_H
#define _CUDA_HELPERS_H

namespace astraCUDA3d {

    // Performs g_k - g_(k-1)
    // The method wants g_k = Wt*R*(W*x -p), but we have computed Wt*R*(p-W*x) and assigned this to g_k,
    // so we are going to switch the operands to g_(k-1) - g_k. The result will be assigned to the now-useless g_(k-1)
    // It computes the sum of squares used for the gamma normalization, additionally.
    __global__ void k_subtractVolumes(cudaPitchedPtr g, cudaPitchedPtr prev_g, float* sumOfSquares, unsigned int nX, unsigned int nY, unsigned int nZ);

    // Computes gamma 
    __global__ void k_BBstep(cudaPitchedPtr x, cudaPitchedPtr prev_x, cudaPitchedPtr prev_g, float* norm_gradients, float* BBStepValue, unsigned int nX, unsigned int nY, unsigned int nZ);

    // Computes the net x_k+1 and assignes (x_k,g_k) to (x_k-1,g_k-1) to be used in the next iteration 
    __global__ void k_lastBBstep(cudaPitchedPtr x, cudaPitchedPtr prev_x, cudaPitchedPtr g, cudaPitchedPtr prev_g, float* BBStepValue, unsigned int nX, unsigned int nY, unsigned int nZ);

    __global__ void mask(cudaPitchedPtr volumePtr, cudaPitchedPtr segmPtr, int nX, int nY, int nZ, 
		  float* g_absorptionValues, int nMaterials );

    __global__ void polyDARTmask(cudaPitchedPtr volumePtr, cudaPitchedPtr segmPtr, int nX, int nY, int nZ, 
	    float* absorptionValuesPtr, float* absorptionThresholdsPtr, int nMaterials );

	bool AttenuationReferenceValues_byAverage(void* D_matAttenuations, void* D_avgMatAttenuations, const SMemoryDimensions3D& spectralInfo);
	bool AttenuationReferenceValues_byWeightedAverage(void* D_energyResponse, void* D_matAttenuations, void* D_avgMatAttenuations, const SMemoryDimensions3D& spectralInfo);
	bool AttenuationReferenceValues_byLogAverage(void* D_energyResponse, void* D_matAttenuations, void* D_avgMatAttenuations, const SMemoryDimensions3D& spectralInfo); //GPU
  bool AttenuationReferenceValues_byLogAverage(const float* energyResponse, const float* matAttenuations, float* avgMatAttenuations, const SMemoryDimensions3D& spectralInfo);    //CPU

  bool FindThresholdReferenceValues(const float* avgMatAttenuations, const float* matPercThresholds, float* matThresholds, const SMemoryDimensions3D& spectralInfo);

  bool SegmentationPolicy( cudaPitchedPtr D_volumeData2, cudaPitchedPtr D_tmpData, const SDimensions3D& dims2, 
		void* D_matAttenuations, void* D_energyResponse, const SMemoryDimensions3D& spectralInfo);

  bool PolyDART_SegmentationPolicy( cudaPitchedPtr D_volumeData2, cudaPitchedPtr D_tmpData, const SDimensions3D& dims2, 
	  void* D_avgMatAttenuations, void* D_matThresholds, const SMemoryDimensions3D& spectralInfo);
}

#endif
