#-----------------------------------------------------------------------
#Copyright 2013 Centrum Wiskunde & Informatica, Amsterdam
#
#Author: Daniel M. Pelt
#Contact: D.M.Pelt@cwi.nl
#Website: http://dmpelt.github.io/pyastratoolbox/
#
#
#This file is part of the Python interface to the
#All Scale Tomographic Reconstruction Antwerp Toolbox ("ASTRA Toolbox").
#
#The Python interface to the ASTRA Toolbox is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#The Python interface to the ASTRA Toolbox is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with the Python interface to the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------

import astra
import numpy as np

# Additional import to support multi-node exectution
import astra.mpi_c as mpi

import time

#time.sleep(14)

vol_geom = astra.create_vol_geom(128, 128, 128)

angles = np.linspace(0, 2*np.pi, 360,False)
proj_geom = astra.create_proj_geom('cone', 1.0, 1.0, 180, 180, angles,300,0)

# Create a simple hollow cube phantom
cube = np.zeros((128,128,128),dtype=np.float32)
#cube[17:113,17:113,17:113] = 0.01
cube[0:128,0:128,0:128] = 0.01
cube[33:97,33:97,33:97] = 0

#cube[0:32,:,:]=0
#cube[64:128,:,:]=0

# Modify the geometry to support distributed execution and then
# proceed as before
proj_geom, vol_geom = mpi.create(proj_geom, vol_geom,0,0)

# Create a spectral information of source/detector and materials
spectralResponse = np.array([0.5, 8, 1, 0.5],dtype=np.float32)
materialAttenuation = np.array([0.01, 0.03, 0.01,0.005],dtype=np.float32)

spectralResponse_id    = astra.data2d.create('-energySpectrum',    None, spectralResponse)
materialAttenuation_id = astra.data2d.create('-attenuationValues', None, materialAttenuation)

# Create projection data from this
proj_id, proj_data = astra.create_sino3d_gpu_poly(cube, proj_geom, vol_geom, spectralResponse_id, materialAttenuation_id)
#proj_id2, proj_data2 = astra.create_sino3d_gpu(cube, proj_geom, vol_geom)

# Display a single projection image
import pylab
pylab.gray()
pylab.figure(1)
pylab.imshow(proj_data[:,45,:])
#pylab.figure(2)
#pylab.imshow(proj_data2[:,33,:])
#pylab.figure(3)
#pylab.imshow(proj_data[:,33,:]-proj_data2[:,33,:])
pylab.show()


# Clean up. Note that GPU memory is tied up in the algorithm object,
# and main RAM in the data objects.
astra.data3d.delete(proj_id)
